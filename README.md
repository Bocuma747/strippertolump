# StripperToLump #

## Applies [Stripper:Source](https://www.bailopan.net/stripper/) config files (.cfg) directly to .BSP files. ##
### Can also export to [.lmp files.](https://developer.valvesoftware.com/wiki/Lump_file_format) ##


### Setup ###
* You must have [Python 3 64-bit](https://www.python.org/downloads/) installed. This program will not work in Python 2, and running it on 32-bit may cause the program to run out of memory.
* Run main.py




### Helpful links ###
* [Stripper:Source](https://www.bailopan.net/stripper/)
* [Source BSP File Format - Valve Developer Community](https://developer.valvesoftware.com/wiki/Source_BSP_File_Format) (contains info on lumps and the bsp file structure)
* [VIDE download](https://developer.valvesoftware.com/wiki/VIDE)
* [AutoStripperConfig](https://bitbucket.org/Bocuma747/autostripperconfig)


For any questions, bug reports, etc., contact me on Discord: Bocuma#2305



License: GNU GPLv3