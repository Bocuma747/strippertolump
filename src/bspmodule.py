import struct
import io


class BSPException(Exception):
    pass


class BSP:
    def __init__(self, bsp):
        self.bsp = io.BytesIO(bsp)

        """
        The first 1036 bytes of a BSP file are its header. The header contains:

            -first 4 bytes:   BSP identifier, consisting of the letters "VBSP" in ASCII

            -next 4 bytes:    the BSP version (integer)
                              https://developer.valvesoftware.com/wiki/Source_BSP_File_Format#Versions

            -next 1024 bytes: an array containing the offset, length, version and fourCC of each
                              of the 64 lumps. Unused lumps have zero for all of these values.
                offset:  offset into file where the lump begins (bytes)
                length:  length of lump (bytes)
                version: lump format version (usually zero)
                fourCC:  4 byte identifier (usually 0, 0, 0, 0)

            -next 4 bytes:    the map revision number (integer)
        """

        LUMPS_V19_AND_PREV = {
            "LUMP_ENTITIES": 0, "LUMP_PLANES": 1, "LUMP_TEXDATA": 2, "LUMP_VERTEXES": 3,
            "LUMP_VISIBILITY": 4, "LUMP_NODES": 5, "LUMP_TEXINFO": 6,
            "LUMP_FACES": 7, "LUMP_LIGHTING": 8, "LUMP_OCCLUSION": 9,
            "LUMP_LEAFS": 10, "LUMP_UNUSED0": 11, "LUMP_EDGES": 12,
            "LUMP_SURFEDGES": 13, "LUMP_MODELS": 14, "LUMP_WORLDLIGHTS": 15,
            "LUMP_LEAFFACES": 16, "LUMP_LEAFBRUSHES": 17, "LUMP_BRUSHES": 18,
            "LUMP_BRUSHSIDES": 19, "LUMP_AREAS": 20, "LUMP_AREAPORTALS": 21,
            "LUMP_PORTALS": 22, "LUMP_CLUSTERS": 23, "LUMP_PORTALVERTS": 24,
            "LUMP_CLUSTERPORTALS": 25, "LUMP_DISPINFO": 26, "LUMP_ORIGINALFACES": 27,
            "LUMP_UNUSED1": 28, "LUMP_PHYSCOLLIDE": 29, "LUMP_VERTNORMALS": 30,
            "LUMP_VERTNORMALINDICES": 31, "LUMP_DISP_LIGHTMAP_ALPHAS": 32, "LUMP_DISP_VERTS": 33,
            "LUMP_DISP_LIGHTMAP_SAMPLE_POSITIONS": 34, "LUMP_GAME_LUMP": 35, "LUMP_LEAFWATERDATA": 36,
            "LUMP_PRIMITIVES": 37, "LUMP_PRIMVERTS": 38, "LUMP_PRIMINDICES": 39,
            "LUMP_PAKFILE": 40, "LUMP_CLIPPORTALVERTS": 41, "LUMP_CUBEMAPS": 42,
            "LUMP_TEXDATA_STRING_DATA": 43, "LUMP_TEXDATA_STRING_TABLE": 44, "LUMP_OVERLAYS": 45,
            "LUMP_LEAFMINDISTTOWATER": 46, "LUMP_FACE_MACRO_TEXTURE_INFO": 47, "LUMP_DISP_TRIS": 48,
            "LUMP_PHYSCOLLIDESURFACE": 49, "LUMP_WATEROVERLAYS": 50, "LUMP_LIGHTMAPPAGES": 51,
            "LUMP_LIGHTMAPPAGEINFOS": 52, "LUMP_UNUSED2": 53, "LUMP_UNUSED3": 54,
            "LUMP_UNUSED4": 55, "LUMP_UNUSED5": 56, "LUMP_XZIPPAKFILE": 57,
            "LUMP_FACES_HDR": 58, "LUMP_UNUSED6": 59, "LUMP_UNUSED7": 60,
            "LUMP_UNUSED8": 61, "LUMP_UNUSED9": 62, "LUMP_UNUSED10": 63}

        LUMPS_V20 = {
            "LUMP_ENTITIES": 0, "LUMP_PLANES": 1, "LUMP_TEXDATA": 2, "LUMP_VERTEXES": 3,
            "LUMP_VISIBILITY": 4, "LUMP_NODES": 5, "LUMP_TEXINFO": 6,
            "LUMP_FACES": 7, "LUMP_LIGHTING": 8, "LUMP_OCCLUSION": 9,
            "LUMP_LEAFS": 10, "LUMP_FACEIDS": 11, "LUMP_EDGES": 12,
            "LUMP_SURFEDGES": 13, "LUMP_MODELS": 14, "LUMP_WORLDLIGHTS": 15,
            "LUMP_LEAFFACES": 16, "LUMP_LEAFBRUSHES": 17, "LUMP_BRUSHES": 18,
            "LUMP_BRUSHSIDES": 19, "LUMP_AREAS": 20, "LUMP_AREAPORTALS": 21,
            "LUMP_UNUSED0": 22, "LUMP_UNUSED1": 23, "LUMP_UNUSED2": 24,
            "LUMP_UNUSED3": 25, "LUMP_DISPINFO": 26, "LUMP_ORIGINALFACES": 27,
            "LUMP_PHYSDISP": 28, "LUMP_PHYSCOLLIDE": 29, "LUMP_VERTNORMALS": 30,
            "LUMP_VERTNORMALINDICES": 31, "LUMP_DISP_LIGHTMAP_ALPHAS": 32, "LUMP_DISP_VERTS": 33,
            "LUMP_DISP_LIGHTMAP_SAMPLE_POSITIONS": 34, "LUMP_GAME_LUMP": 35, "LUMP_LEAFWATERDATA": 36,
            "LUMP_PRIMITIVES": 37, "LUMP_PRIMVERTS": 38, "LUMP_PRIMINDICES": 39,
            "LUMP_PAKFILE": 40, "LUMP_CLIPPORTALVERTS": 41, "LUMP_CUBEMAPS": 42,
            "LUMP_TEXDATA_STRING_DATA": 43, "LUMP_TEXDATA_STRING_TABLE": 44, "LUMP_OVERLAYS": 45,
            "LUMP_LEAFMINDISTTOWATER": 46, "LUMP_FACE_MACRO_TEXTURE_INFO": 47, "LUMP_DISP_TRIS": 48,
            "LUMP_UNUSED4": 49, "LUMP_WATEROVERLAYS": 50, "LUMP_LEAF_AMBIENT_INDEX_HDR": 51,
            "LUMP_LEAF_AMBIENT_INDEX": 52, "LUMP_LIGHTING_HDR": 53, "LUMP_WORLDLIGHTS_HDR": 54,
            "LUMP_LEAF_AMBIENT_LIGHTING_HDR": 55, "LUMP_LEAF_AMBIENT_LIGHTING": 56, "LUMP_XZIPPAKFILE": 57,
            "LUMP_FACES_HDR": 58, "LUMP_MAP_FLAGS": 59, "LUMP_OVERLAY_FADES": 60,
            "LUMP_OVERLAY_SYSTEM_LEVELS": 61, "LUMP_UNUSED5": 62, "LUMP_UNUSED6": 63}

        LUMPS_V21 = {
            "LUMP_ENTITIES": 0, "LUMP_PLANES": 1, "LUMP_TEXDATA": 2, "LUMP_VERTEXES": 3,
            "LUMP_VISIBILITY": 4, "LUMP_NODES": 5, "LUMP_TEXINFO": 6,
            "LUMP_FACES": 7, "LUMP_LIGHTING": 8, "LUMP_OCCLUSION": 9,
            "LUMP_LEAFS": 10, "LUMP_FACEIDS": 11, "LUMP_EDGES": 12,
            "LUMP_SURFEDGES": 13, "LUMP_MODELS": 14, "LUMP_WORLDLIGHTS": 15,
            "LUMP_LEAFFACES": 16, "LUMP_LEAFBRUSHES": 17, "LUMP_BRUSHES": 18,
            "LUMP_BRUSHSIDES": 19, "LUMP_AREAS": 20, "LUMP_AREAPORTALS": 21,
            "LUMP_PROPCOLLISION": 22, "LUMP_PROPHULLS": 23, "LUMP_PROPHULLVERTS": 24,
            "LUMP_PROPTRIS": 25, "LUMP_DISPINFO": 26, "LUMP_ORIGINALFACES": 27,
            "LUMP_PHYSDISP": 28, "LUMP_PHYSCOLLIDE": 29, "LUMP_VERTNORMALS": 30,
            "LUMP_VERTNORMALINDICES": 31, "LUMP_DISP_LIGHTMAP_ALPHAS": 32, "LUMP_DISP_VERTS": 33,
            "LUMP_DISP_LIGHTMAP_SAMPLE_POSITIONS": 34, "LUMP_GAME_LUMP": 35, "LUMP_LEAFWATERDATA": 36,
            "LUMP_PRIMITIVES": 37, "LUMP_PRIMVERTS": 38, "LUMP_PRIMINDICES": 39,
            "LUMP_PAKFILE": 40, "LUMP_CLIPPORTALVERTS": 41, "LUMP_CUBEMAPS": 42,
            "LUMP_TEXDATA_STRING_DATA": 43, "LUMP_TEXDATA_STRING_TABLE": 44, "LUMP_OVERLAYS": 45,
            "LUMP_LEAFMINDISTTOWATER": 46, "LUMP_FACE_MACRO_TEXTURE_INFO": 47, "LUMP_DISP_TRIS": 48,
            "LUMP_PROP_BLOB": 49, "LUMP_WATEROVERLAYS": 50, "LUMP_LEAF_AMBIENT_INDEX_HDR": 51,
            "LUMP_LEAF_AMBIENT_INDEX": 52, "LUMP_LIGHTING_HDR": 53, "LUMP_WORLDLIGHTS_HDR": 54,
            "LUMP_LEAF_AMBIENT_LIGHTING_HDR": 55, "LUMP_LEAF_AMBIENT_LIGHTING": 56, "LUMP_XZIPPAKFILE": 57,
            "LUMP_FACES_HDR": 58, "LUMP_MAP_FLAGS": 59, "LUMP_OVERLAY_FADES": 60,
            "LUMP_OVERLAY_SYSTEM_LEVELS": 61, "LUMP_PHYSLEVEL": 62, "LUMP_DISP_MULTIBLEND": 63}

        # BSP identifier
        self.ident = self.bsp.read(4).decode()
        if self.ident != "VBSP":
            raise BSPException(
                "Invalid BSP file (first 4 bytes not equal to \"VBSP\")")

        # BSP version. Should be 19-21 in all Source games
        # except Vampire: The Masquerade – Bloodlines, which uses v17.
        self.version = struct.unpack('i', self.bsp.read(4))[0]
        if self.version <= 19:
            self.lumpslist = LUMPS_V19_AND_PREV
        elif self.version == 20:
            self.lumpslist = LUMPS_V20
        elif self.version == 21:
            self.lumpslist = LUMPS_V21
        else:
            raise BSPException("Unsupported BSP version: %s" % self.version)

        # Lump directory array
        self.lumps_array = []
        for i in range(len(self.lumpslist)):
            self.lump_t = []
            for i in struct.unpack('3i', self.bsp.read(12)):
                self.lump_t.append(i)
            self.lump_t.append(struct.unpack('4b', self.bsp.read(4)))
            self.lumps_array.append(self.lump_t)

        # Map revision number
        self.maprevision = struct.unpack('i', self.bsp.read(4))[0]

        self.header = [self.ident,
                       self.version,
                       self.lumps_array,
                       self.maprevision]

    def get_lump(self, lump):

        """
        Returns the raw lump bytes. Lump can be chosen by its name or index number, both of which
        are listed here: https://developer.valvesoftware.com/wiki/Source_BSP_File_Format#Lump_types
        """

        # Assume input is valid unless proven otherwise.
        input_valid = True
        # If the input is an integer, make sure it's in range.
        if isinstance(lump, int):
            index = int(lump)
            if index > len(self.lumpslist) - 1 or index < 0:
                raise BSPException("Index out of range: %d" % index)
                input_valid = False
        # If the input is a string, get its numerical index from the dictionary.
        elif isinstance(lump, str):
            try:
                index = self.lumpslist[lump]
            except KeyError:
                raise BSPException("Lump %s not found" % lump)
                input_valid = False
        # If the input is neither string nor int, throw an exception.
        else:
            raise BSPException("Bad input type, must be int or str")
            input_valid = False

        if input_valid:
            self.offset = self.lumps_array[index][0]
            self.length = self.lumps_array[index][1]
            self.lumprevision = self.lumps_array[index][2]
            self.fourCC = self.lumps_array[index][3]

            self.bsp.seek(self.offset, 0)
            self.lump = self.bsp.read(self.length)

            return self.lump

    def get_lump_version(self, lump):

        """
        Returns the lump's version. Lump can be chosen by its name or index number.
        """

        # Assume input is valid unless proven otherwise.
        input_valid = True
        # If the input is an integer, make sure it's in range.
        if isinstance(lump, int):
            index = int(lump)
            if index > len(self.lumpslist) - 1 or index < 0:
                raise BSPException("Index out of range: %d" % index)
                input_valid = False
        # If the input is a string, get its numerical index from the dictionary.
        elif isinstance(lump, str):
            try:
                index = self.lumpslist[lump]
            except KeyError:
                raise BSPException("Lump %s not found" % lump)
                input_valid = False
        # If the input is neither string nor int, throw an exception.
        else:
            raise BSPException("Bad input type, must be int or str")
            input_valid = False

        if input_valid:
            self.offset = self.lumps_array[index][0]
            self.length = self.lumps_array[index][1]
            self.lumprevision = self.lumps_array[index][2]
            self.fourCC = self.lumps_array[index][3]

            return self.lumprevision

    def get_version(self):
        return self.version

    def get_mapRevision(self):
        return self.maprevision
