import re
import os
import struct
import shutil
import threading
import io
import datetime
from timeit import default_timer as timer
from tkinter import Tk, filedialog, ttk, font, messagebox, scrolledtext, Grid, Toplevel, IntVar
import readlump
import readstripper
import bspmodule


class WriteLump:
    def __init__(self, lump, bsp):
        self.lump = lump
        self.bsp = bsp
        self.BSPinfo = bspmodule.BSP(self.bsp)

    def writelmp(self):

        bytes_out = io.BytesIO()

        self.header = struct.pack(
            'iiiii',
            # Lump header length; always = 20
            20,
            # Lump ID; always = 0
            0,
            # Lump version; always = 0
            0,
            # Length of new lump, not including this header
            len(self.lump.encode('ascii')),
            # Map version; unknown so we'll just write 1
            1)

        bytes_out.write(self.header + self.lump.encode('ascii'))

        return bytes_out.getvalue()

    def writebsp(self):

        header = io.BytesIO()
        lumps = io.BytesIO()

        # Create and write the identifier (the letters VBSP in ascii)
        ident = "VBSP".encode('ascii')
        header.write(ident)
        header.seek(0, 2)

        # Get and write the bsp's version
        bsp_version = self.BSPinfo.get_version()
        header.write(struct.pack('i', bsp_version))
        header.seek(0, 2)

        entlump_new = self.lump

        # Write the new entity lump's info to the bsp header
        offset = struct.pack('i', 1036)
        length = struct.pack('i', len(entlump_new.encode('ascii')))
        lumpversion = struct.pack('i', 0)
        fourCC = struct.pack('4b', 0, 0, 0, 0)
        header.write(offset + length + lumpversion + fourCC)

        # Write the new entity lump
        lumps.write(entlump_new.encode('ascii'))

        # Now we write the rest of the lumps, and their info to the bsp header
        # Start at lump #1 (the lump after LUMP_ENTITIES)
        for i in range(1, 64):
            lumps.seek(0, 2)
            header.seek(0, 2)
            lumpdata = self.BSPinfo.get_lump(i)
            offset = (lumps.seek(0, 2) + 1036)
            length = struct.pack('i', len(lumpdata))
            lumpversion = struct.pack('i', self.BSPinfo.get_lump_version(i))
            fourCC = struct.pack('4b', 0, 0, 0, 0)

            # We have to make a special modification to the game lump (lump #35)
            if i == 35:
                gamelump = io.BytesIO(lumpdata)
                new_gamelump = io.BytesIO()

                gamelump.seek(0, 0)
                # Write the gamelump count, the number of lumps within the gamelump
                gamelumpcount = struct.unpack('i', gamelump.read(4))[0]
                new_gamelump.write(struct.pack('i', gamelumpcount))

                ##### Write the first gamelump directory array #####
                new_gamelump.seek(0, 2)
                # Write the first gamelump ID, the flags, and the gamelump version (8 bytes total)
                new_gamelump.write(gamelump.read(8))
                new_gamelump.seek(0, 2)
                # Write the new offset of the first gamelump.
                # This is the whole gamelump's offset + 4 bytes (the gamelump count) + each gamelump header (16 bytes each)
                gamelump_offset = offset+4+(gamelumpcount*16)
                new_gamelump.write(struct.pack('i', gamelump_offset))
                new_gamelump.seek(0, 2)
                # Skip past the old offset
                gamelump.read(4)
                # Write the gamelump's size
                gamelump_size = gamelump.read(4)
                new_gamelump.write(gamelump_size)

                ##### Write the rest of the gamelump directory arrays #####
                for gamelumpnum in range(gamelumpcount - 1):
                    new_gamelump.seek(0, 2)
                    # Write the first gamelump ID, the flags & the gamelump version (8 bytes total)
                    new_gamelump.write(gamelump.read(8))
                    new_gamelump.seek(0, 2)
                    # Write the new offset of this gamelump from the start of the bsp.
                    # This is the previous gamelump's offset + the previous gamelump's size.
                    gamelump_offset += struct.unpack('i', gamelump_size)[0]
                    new_gamelump.write(struct.pack('i', gamelump_offset))
                    new_gamelump.seek(0, 2)
                    # Skip past the old offset
                    gamelump.read(4)
                    # Write the gamelump's size
                    gamelump_size = gamelump.read(4)
                    new_gamelump.write(gamelump_size)

                # Write the actual game lumps
                new_gamelump.seek(0, 2)
                new_gamelump.write(gamelump.read())
                # Write the whole gamelump to the bsp
                lumps.write(new_gamelump.getvalue())
                # Write this lump's info to the main bsp header
                header.write(struct.pack('i', offset) + struct.pack('i', len(new_gamelump.getvalue())) + lumpversion + fourCC)

            else:
                lumps.write(lumpdata)
                header.write(struct.pack('i', offset) + length + lumpversion + fourCC)

        # Write the map's revision number. (The old number + 1)
        mapRevision = self.BSPinfo.get_mapRevision() + 1
        header.seek(0, 2)
        header.write(struct.pack('i', mapRevision))

        bytes_out = io.BytesIO(header.getvalue() + lumps.getvalue())
        return bytes_out


class Console:
    def __init__(self, master):

        def copyall():
            self.textbox_contents = self.textbox.get("1.0", 'end-1c')
            root.clipboard_clear()
            root.clipboard_append(self.textbox_contents)

        self.errorcount = 0
        self.warningcount = 0
        self.errorslist = []
        self.warningslist = []

        self.master = master
        master.title("Stripper to Lump")
        Grid.rowconfigure(master, 0, weight=1)
        Grid.columnconfigure(master, 0, weight=1)
        Grid.columnconfigure(master, 1, weight=1)

        self.textbox = scrolledtext.ScrolledText(master,
                                                 wrap="word",
                                                 state="disabled")
        self.textbox.grid(row=0, column=0, columnspan=2, sticky="nsew")
        self.textbox.tag_configure("green", foreground="green")
        self.textbox.tag_configure("red", foreground="red")
        self.textbox.tag_configure("orange", foreground="orange")
        self.textbox.tag_configure("black", foreground="black")

        self.button_copylog = ttk.Button(master,
                                         text="Copy All",
                                         command=copyall)
        self.button_copylog.grid(row=1, column=0, sticky="nsew")

        self.button_closewindow = ttk.Button(master,
                                             text="Close",
                                             command=master.destroy)
        self.button_closewindow.grid(row=1, column=1, sticky="nsew")

        # Window geometry
        master.update()
        self.screenwidth = master.winfo_screenwidth()
        self.screenheight = master.winfo_screenheight()
        self.initwindowsize = "%dx%d" % (self.screenwidth/2,
                                         self.screenheight/1.5)
        master.geometry(self.initwindowsize)

    def output(self, message, flag):
        if flag == "n":     # "n" = normal
            color = "black"
        elif flag == "g":   # "g" = green text
            color = "green"
        elif flag == "w":   # "w" = warning
            color = "orange"
            self.warningcount += 1
            self.warningslist.append(message)
        elif flag == "e":   # "e" = error
            color = "red"
            self.errorcount += 1
            self.errorslist.append(message)
        # Set color to black if an invalid flag is given
        else:
            color = "black"

        # Write the message to textbox
        self.textbox.configure(state="normal")
        self.textbox.insert("end", message, color)
        self.textbox.see("end")
        self.textbox.configure(state="disabled")


class RemoveDelimiters:
    """With Stripper's regex syntax, you have to put delimiters
    (forward slashes) at the start and end of each key or value.
    Python doesn't recognize these so we have to strip them out if they exist,
    otherwise it will interpret them literally.
    """
    def __init__(self, kv):
        self.key_has_regex = False
        self.value_has_regex = False
        self.kv_has_regex = False
        # self.has_regex checks a key or value for whether it has regex delimiters
        self.has_regex = re.compile("\"\/(.*)\/\"")
        self.kv = ""

        self.key = kv.split(' ', 1)[0]
        self.trymatch_key = self.has_regex.match(self.key)
        if self.trymatch_key:
            self.key_has_regex = True
            self.kv_has_regex = True
            self.key = "\"%s\"" % self.trymatch_key.group(1)
        else:
            self.key_has_regex = False
            self.key = self.key
        self.kv += self.key
        self.kv += " "
        self.value = kv.split(' ', 1)[1]
        self.trymatch_value = self.has_regex.match(self.value)
        if self.trymatch_value:
            self.value_has_regex = True
            self.kv_has_regex = True
            self.value = "\"%s\"" % self.trymatch_value.group(1)
        else:
            self.value_has_regex = False
            self.value = self.value
        self.kv += self.value


class Convert:
    """Apply the Stripper file to the ent lump.
    """
    def __init__(self, stripper, lump, consolewindow, bspname):

        consolewindow.errorcount = 0
        consolewindow.warningcount = 0
        consolewindow.errorslist = []
        consolewindow.warningslist = []

        self.bspname = bspname
        start = timer()

        ReadStripperConfig = readstripper.ReadStripper(stripper, consolewindow)
        ReadLump = readlump.ReadLump(lump)

        """Make new_blocklist, a copy of the lump's original block list. This is what we'll edit.
        A block is a set of {curly braces} with keyvalues, one per line, between them.
        In lumps, they are one single entity each. In Stripper files, they are one single item each.
        """
        new_blocklist = []
        for lumpblock in ReadLump.parse():
            new_blocklist.append(lumpblock)

        # Iterate through each Stripper block:
        for stripperblock in ReadStripperConfig.parse():
            mode = stripperblock[0]
            properties = stripperblock[1]

            # ADD
            if mode == 'add':
                new_blocklist.append(properties)
                consolewindow.output(("> AddBlock: Added block with "
                                      "properties %s to end of lump.\n\n") % properties, "n")

            # FILTER
            if mode == 'filter':
                for lumpblock_num, lumpblock in enumerate(new_blocklist):
                    filterthis = True
                    for prop in properties:
                        strip_delims = RemoveDelimiters(prop)
                        kv = strip_delims.kv
                        pattern = re.compile(kv)
                        # If the keyvalue has a regex, search for it in the lump with regex
                        if (strip_delims.kv_has_regex
                                and list(filter(pattern.search, lumpblock)) == []):
                            filterthis = False
                            break
                        # If the keyvalue doesn't have a regex, search for it literally
                        elif (not strip_delims.kv_has_regex
                                and kv not in lumpblock):
                            filterthis = False
                            break
                    # If we find a match, re-make new_blocklist but without this block
                    if filterthis:
                        new_blocklist = [x for x in new_blocklist if x != lumpblock]
                        consolewindow.output(("> FilterBlock: Filtered block "
                                              "#%d from lump\n\n") % (lumpblock_num+1),
                                             "n")

            # MODIFY
            elif mode == 'modify':
                for lumpblock_num, lumpblock in enumerate(new_blocklist):
                    match = True

                    # A modify block without a match sub-block gets applied to all entities.
                    if stripperblock[1].get("match", None) == []:
                        match = True
                    # If it does have a match sub-block, then apply only to matching blocks.
                    else:
                        for MatchSubblock_prop in stripperblock[1].get("match", None):
                            strip_delims = RemoveDelimiters(MatchSubblock_prop)
                            MatchSubblock_prop_pattern = re.compile(strip_delims.kv)
                            # If the keyvalue has a regex, search for it in the lump with regex
                            if strip_delims.kv_has_regex:
                                if list(filter(MatchSubblock_prop_pattern.search, lumpblock)) == []:
                                    match = False
                                    break
                            # If the keyvalue doesn't have a regex, search for it literally
                            elif not strip_delims.kv_has_regex:
                                if strip_delims.kv not in lumpblock:
                                    match = False
                                    break

                    if match:

                        # REPLACE
                        if stripperblock[1].get("replace", None) != []:
                                    for subblock_prop in stripperblock[1].get("replace", None):
                                        strip_delims = RemoveDelimiters(subblock_prop)
                                        subblock_key_pattern = re.compile(strip_delims.key)
                                        subblock_value = subblock_prop.split(' ', 1)[1]
                                        for prop_num, lump_prop in enumerate(lumpblock):
                                            lump_key = lump_prop.split(' ', 1)[0]
                                            lump_value = lump_prop.split(' ', 1)[1]
                                            # If the key has a regex,
                                            # search for it in the lump with regex
                                            if strip_delims.key_has_regex:
                                                if subblock_key_pattern.match(lump_key):
                                                    lumpblock[prop_num] = "%s %s" % (lump_key,
                                                                                     subblock_value)
                                                    consolewindow.output(
                                                        ("> Lump block #%d: Replaced \"%s %s\""
                                                         " with \"%s %s\"\n\n") % (
                                                             (lumpblock_num+1),
                                                             lump_key,
                                                             lump_value,
                                                             lump_key,
                                                             subblock_value),
                                                        "n")
                                            # If the key doesn't have a regex,
                                            # search for it literally
                                            elif not strip_delims.key_has_regex:
                                                if strip_delims.key == lump_key:
                                                    lumpblock[prop_num] = "%s %s" % (lump_key,
                                                                                     subblock_value)
                                                    consolewindow.output(
                                                        ("> Lump block #%d: Replaced \"%s %s\""
                                                         " with \"%s %s\"\n\n") % (
                                                             (lumpblock_num+1),
                                                             lump_key,
                                                             lump_value,
                                                             lump_key,
                                                             subblock_value),
                                                        "n")

                        # DELETE
                        if stripperblock[1].get("delete", None) != []:
                                    for DeleteSubblock_prop in stripperblock[1].get("delete", None):
                                        strip_delims = RemoveDelimiters(DeleteSubblock_prop)
                                        DeleteSubblock_prop_pattern = re.compile(strip_delims.kv)
                                        for prop_num, lump_prop in enumerate(lumpblock):
                                            # If the keyvalue has a regex,
                                            # search for it in the lump with regex
                                            if strip_delims.kv_has_regex:
                                                # If matched, re-make this lumpblock
                                                # but without the matched keyvalue
                                                if DeleteSubblock_prop_pattern.match(lump_prop):
                                                    lumpblock = [
                                                        x for x in lumpblock if x != lump_prop]
                                                    consolewindow.output(
                                                        "> Lump block #%d: Deleted \"%s\"\n\n" % (
                                                            (lumpblock_num+1),
                                                            lump_prop),
                                                        "n")
                                            # If the keyvalue doesn't have a regex,
                                            # search for it literally
                                            elif not strip_delims.kv_has_regex:
                                                # If matched, re-make this lumpblock
                                                # but without the matched keyvalue
                                                if strip_delims.kv == lump_prop:
                                                    lumpblock = [
                                                        x for x in lumpblock if x != lump_prop]
                                                    consolewindow.output(
                                                        "> Lump block #%d: Deleted \"%s\"\n\n" % (
                                                            (lumpblock_num+1),
                                                            lump_prop),
                                                        "n")

                        # INSERT
                        if stripperblock[1].get("insert", None) != []:
                                    for InsertSubblock_prop in stripperblock[1].get("insert", None):
                                        lumpblock.append(InsertSubblock_prop)
                                        consolewindow.output(
                                            "> Lump block #%d: Inserted \"%s\"\n\n" % (
                                                (lumpblock_num+1),
                                                InsertSubblock_prop),
                                            "n")

                        new_blocklist[lumpblock_num] = lumpblock

        # Now that we're done converting, generate and format the new lumpblocks.
        self.new_blocklist = new_blocklist

        self.lump = ""
        for block in self.new_blocklist:
            self.lump += "{\n"
            for prop in block:
                self.lump += "%s\n" % prop
            self.lump += "}\n"

        # Include newlines in byte count
        self.lump_len = len(self.lump) + self.lump.count('\n')

        end = timer()
        consolewindow.output("\nJob done.\n", "g")

        # Make the text for the error and warning count
        if consolewindow.errorcount == 1:
            errorstring = "error"
        else:
            errorstring = "errors"
        if consolewindow.warningcount == 1:
            warningstring = "warning"
        else:
            warningstring = "warnings"

        # If there were any errors or warnings, write them to a .log in the cwd
        if consolewindow.errorcount > 0 or consolewindow.warningcount > 0:
            errorlog_name = "errorlog-"+self.bspname+"-"+datetime.datetime.now().strftime("%d_%m_%Y_%H%M_%S")+".log"
            with open(os.path.join(os.getcwd(), errorlog_name), "w") as errorlog:
                errorlog.write("Map name: %s\n\n" % self.bspname)
                errorlog.write("==================== Warnings ====================\n\n")
                for warning in consolewindow.warningslist:
                    errorlog.write(warning+"\n")
                errorlog.write("==================== Errors ====================\n\n")
                for error in consolewindow.errorslist:
                    errorlog.write(error+"\n")
            consolewindow.output("Wrote error log %s\n" % os.path.join(
                os.getcwd(),
                errorlog_name), "n")

        consolewindow.output("%d %s\n" % (consolewindow.errorcount,
                                          errorstring), "n")
        consolewindow.output("%d %s\n" % (consolewindow.warningcount,
                                          warningstring), "n")
        timecount_seconds = (end - start)
        m, s = divmod(timecount_seconds, 60)
        consolewindow.output("Time elapsed: %02d:%02d\n" % (m, s), "n")

    def getlump(self):
        return self.lump


class Main:
    # GUI
    def __init__(self, master):
        lumpfile_name_regex = re.compile("(.*)_l_(\d*)")

        def convert():
            self.newwindow = Toplevel(self.master)
            consolewindow = Console(self.newwindow)

            # For each Stripper file, iterate through each bsp
            for strippertree_entry in self.treeview_StripperFileList.get_children():
                stripperfile_path = os.path.abspath(
                    self.treeview_StripperFileList.item(
                        strippertree_entry)['text'])
                stripperfile_name = os.path.splitext(
                    os.path.basename(
                        stripperfile_path))[0]

                for bsptree_entry in self.treeview_BspList.get_children():
                    bsp_path = os.path.abspath(self.treeview_BspList.item(bsptree_entry)['text'])
                    bsp_name = os.path.splitext(os.path.basename(bsp_path))[0]

                    # When we find the bsp that matches the Stripper file, open both.
                    if bsp_name == stripperfile_name:
                        with open(stripperfile_path, "r") as stripperfile:
                            with open(bsp_path, "rb+") as bspfile:
                                bspfile_read = bspfile.read()
                                bsp_entlump = bspmodule.BSP(bspfile_read)
                                lump = bsp_entlump.get_lump(0)

                                # Apply the stripper to the bsp's ent lump.
                                consolewindow.output(("\n================ "
                                                      "Applying %s.cfg to %s.bsp "
                                                      "================\n") % (stripperfile_name,
                                                                               bsp_name),
                                                     "n")
                                print("Converting: %s.bsp" % bsp_name)
                                try:
                                    self.convert = Convert(stripperfile,
                                                           lump,
                                                           consolewindow,
                                                           bsp_name)

                                    self.new_lump = self.convert.getlump()

                                    WriteLumpInst = WriteLump(lump=self.new_lump, bsp=bspfile_read)

                                    # If the user chose to output to the BSP:
                                    if self.var_outputmode.get() == 2:
                                        # Make a backup if the option to do so is selected
                                        if self.var_makebackup.get() == 1:
                                            shutil.copy(bsp_path, os.path.join(os.path.dirname(bsp_path), bsp_name+".bsp.backup"))
                                            consolewindow.output("Made backup of %s.bsp" % bsp_name, "n")
                                        # Write the bsp
                                        with open(bsp_path, "wb") as output:
                                            output.write(WriteLumpInst.writebsp().getvalue())

                                    # If the user chose to output to a .lmp:
                                    else:
                                        # Create the filename for what we're writing.
                                        # If there's already a lumpfile with the same name in outputdir,
                                        # increase the number at the end of the filename by 1.
                                        lumpfile_num = 0
                                        biggestnum = 0
                                        for file in os.listdir(self.outputdir):
                                            if (lumpfile_name_regex.match(file) and
                                                    lumpfile_name_regex.match(file).group(1) == bsp_name):
                                                if biggestnum < int(
                                                        lumpfile_name_regex.match(file).group(2))+1:
                                                    lumpfile_num = int(
                                                        lumpfile_name_regex.match(file).group(2))+1
                                                    biggestnum = lumpfile_num
                                        new_lump_path = os.path.abspath(os.path.join(self.outputdir,
                                                                                     "%s_l_%d.lmp" % (
                                                                                         bsp_name,
                                                                                         lumpfile_num)
                                                                                     )
                                                                        )
                                        # Write the lumpfile
                                        with open(new_lump_path, "wb") as newlumpfile_binary:
                                            newlumpfile_binary.write(WriteLumpInst.writelmp())
                                except error as e:
                                    consolewindow.output("ERROR:\n%s" % e)

        def run():
            if not self.outputdir and self.var_outputmode.get() == 1:
                messagebox.showerror(
                    "Error",
                    "You must select an output directory.",
                    parent=master)
            else:
                self.thread = threading.Thread(target=convert)
                self.thread.start()

        def openfile_stripper():
            StripperFilepath = filedialog.askopenfilename(
                title="Select Stripper file(s)",
                filetypes=[("Stripper config files (.cfg)", "*.cfg")],
                multiple=True,
                parent=master)
            for filepath in StripperFilepath:
                self.treeview_StripperFileList.insert(
                    '',
                    'end',
                    text=os.path.abspath(filepath))

        def openfile_bsp():
            bsppath = filedialog.askopenfilename(
                title="Select .bsp file(s)",
                filetypes=[("BSP files (.bsp)", "*.bsp")],
                multiple=True,
                parent=master)
            for filepath in bsppath:
                self.treeview_BspList.insert(
                    '',
                    'end',
                    text=os.path.abspath(filepath))

        def chooseoutputdir():
            self.outputdir = os.path.abspath(filedialog.askdirectory(
                title="Select output directory",
                parent=master))
            if self.outputdir == "":
                self.outputdir = None
            else:
                self.label_OutputDir = ttk.Label(
                    master,
                    text=self.outputdir,
                    background="white",
                    style="TLabel").grid(row=9, column=0, columnspan=2,
                                         sticky="nsew", padx=10, pady=10)

        def removeselected(tree):
            selection = tree.selection()
            for i in selection[::-1]:
                tree.delete(i)

        def buttoncallback():
            # If user selects the "Write to .lmp file" button, uncheck & disable the backup button.
            if self.var_outputmode.get() == 1:
                self.var_makebackup.set(0)
                self.checkbutton_Makebackup.config(state="disabled")
            # If user selects the "Write directly to BSP" button, enable the backup button.
            else:
                self.checkbutton_Makebackup.config(state="!disabled")

        # GUI
        self.master = master
        master.title("Stripper to Lump")
        Grid.rowconfigure(master, 1, weight=1)
        Grid.rowconfigure(master, 5, weight=1)
        Grid.columnconfigure(master, 1, weight=1)
        self.style = ttk.Style()
        self.fontSegoe11 = font.Font(family="Segoe UI", size=11)
        self.style.configure("Size11.TButton", font=self.fontSegoe11)

        self.outputdir = None

        ttk.Button(master,
                   text="Select Stripper Files...",
                   command=openfile_stripper).grid(row=0, column=0, columnspan=3,
                                                   sticky="w", padx=10, pady=20)

        self.treeview_StripperFileList = ttk.Treeview(master, show="tree")
        self.treeview_StripperFileList.grid(row=1, column=0, columnspan=2,
                                            sticky="nsew", padx=(10, 0))
        self.sb_x_1 = ttk.Scrollbar(master,
                                    command=self.treeview_StripperFileList.xview,
                                    orient="horizontal")
        self.sb_x_1.grid(row=2, column=0, columnspan=2, sticky="ew", padx=10)
        self.sb_y_1 = ttk.Scrollbar(master,
                                    command=self.treeview_StripperFileList.yview)
        self.sb_y_1.grid(row=1, column=2, sticky="ns", padx=(0, 10))
        self.treeview_StripperFileList.configure(xscrollcommand=self.sb_x_1.set,
                                                 yscrollcommand=self.sb_y_1.set)

        ttk.Button(master,
                   text="Remove Selected",
                   command=lambda: removeselected(self.treeview_StripperFileList)).grid(
                       row=3,
                       column=0,
                       columnspan=2,
                       sticky="e",
                       padx=10,
                       pady=20)

        ttk.Button(master,
                   text="Select .bsp Files...",
                   command=openfile_bsp).grid(
                       row=4,
                       column=0,
                       columnspan=3,
                       sticky="w",
                       padx=10,
                       pady=(10, 0))

        self.treeview_BspList = ttk.Treeview(master, show="tree")
        self.treeview_BspList.grid(row=5, column=0, columnspan=2,
                                   sticky="nsew", padx=(10, 0), pady=(20, 0))
        self.sb_x_2 = ttk.Scrollbar(master,
                                    command=self.treeview_BspList.xview,
                                    orient="horizontal")
        self.sb_x_2.grid(row=6, column=0, columnspan=2,
                         sticky="ew", padx=10, pady=(0, 20))
        self.sb_y_2 = ttk.Scrollbar(master,
                                    command=self.treeview_BspList.yview)
        self.sb_y_2.grid(row=5, column=2, sticky="ns",
                         padx=(0, 10), pady=(20, 0))
        self.treeview_BspList.configure(xscrollcommand=self.sb_x_2.set,
                                        yscrollcommand=self.sb_y_2.set)

        ttk.Button(master,
                   text="Remove Selected",
                   command=lambda: removeselected(self.treeview_BspList)).grid(row=7,
                                                                               column=0,
                                                                               columnspan=2,
                                                                               sticky="e",
                                                                               padx=10)

        ttk.Button(master,
                   text="Choose Output Directory (for .lmp files)",
                   command=chooseoutputdir).grid(row=8,
                                                 column=0,
                                                 columnspan=3,
                                                 sticky="w",
                                                 padx=10,
                                                 pady=10)

        self.label_OutputDir = ttk.Label(master,
                                         text="",
                                         background="white",
                                         style="TLabel").grid(row=9,
                                                              column=0,
                                                              columnspan=2,
                                                              sticky="nsew",
                                                              padx=10,
                                                              pady=10)

        # Buttons & vars to set the output mode (whether to write to a .lmp or directly to the bsp)
        self.var_outputmode = IntVar()
        self.var_outputmode.set(2)
        self.var_makebackup = IntVar()
        self.var_makebackup.set(1)

        self.rbutton_Writelmp = ttk.Radiobutton(master,
                                                text="Write to .lmp file(s)",
                                                value=1,
                                                variable=self.var_outputmode,
                                                command=buttoncallback)
        self.rbutton_Writelmp.grid(row=10, column=0, sticky="w", padx=20, pady=20)

        self.rbutton_Writebsp = ttk.Radiobutton(master,
                                                text="Write directly to original .BSP",
                                                value=2,
                                                variable=self.var_outputmode,
                                                command=buttoncallback)
        self.rbutton_Writebsp.grid(row=11, column=0, sticky="w", padx=20)

        self.checkbutton_Makebackup = ttk.Checkbutton(master,
                                                      text="Backup .bsp file(s)",
                                                      variable=self.var_makebackup)
        self.checkbutton_Makebackup.grid(row=12, column=0, padx=60, pady=5)

        ttk.Separator(orient="horizontal").grid(row=13,
                                                column=0,
                                                columnspan=3,
                                                sticky="ew",
                                                pady=10)

        ttk.Button(master,
                   text="Go",
                   command=run).grid(row=14,
                                     column=0,
                                     columnspan=2,
                                     sticky="ns",
                                     padx=20,
                                     pady=(0, 20))

        # Window geometry
        master.update()
        self.screenwidth = master.winfo_screenwidth()
        self.screenheight = master.winfo_screenheight()
        self.initwindowsize = "%dx%d" % (self.screenwidth/3,
                                         self.screenheight/1.5)
        master.geometry(self.initwindowsize)


if __name__ == '__main__':
    root = Tk()
    RUN_GUI = Main(root)
    root.mainloop()
