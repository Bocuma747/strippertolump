import re


class ReadStripper:
    """This class reads a Stripper:Source config file and returns a formatted list with every block
    that the config file contains.

    A "block" is a single item in the Stripper config, consisting of a mode
    (add:, filter: or modify:) and a set of keyvalues, one per line, in curly braces. For example:

        add:
        {
            "classname" "info_player_start"
            "origin" "123 456 789"
        }


    Each block becomes a list whose 1st item is a string that says
    what kind of block it is ('add', 'filter' or 'modify').

    In 'add' and 'filter' blocks, the 2nd item in the list is a list of
    the block's keyvalues (such as "origin" "1 2 3").

    For example, this Stripper block:

            add:
            {
                "classname" "trigger_teleport"
                "origin" "123 456 789"
            }

    becomes
            ['add', ['"classname" "trigger_teleport"', '"origin" "123 456 789"']].


    Note: "remove" is aliased with "filter".

    In 'modify' blocks, the 2nd item in the list is a dictionary with one list per sub-block
    (match:, replace:, delete: and insert:).
    Each one of those lists contains its corresponding keyvalues.

    For example, this Stripper block:

            modify:
            {
                match:
                {
                "origin" "1 2 3"
                "classname" "trigger_teleport"
                }
                replace:
                {
                "target" "mapstart"
                }
            }

    becomes
            ["modify", {"match": ['"origin" "1 2 3"', '"classname" "trigger_teleport"'], "replace": ['"target" "mapstart"'], "delete": [], "insert": []}]


    It's an ugly system but it works.
    """

    def __init__(self, file, consolewindow):
        self.file = file
        self.consolewindow = consolewindow

    def parse(self):

        # NOTE: I use "prop" and "keyvalue" interchangeably here.

        # This regex matches keyvalues in their proper format.
        property_regex = re.compile("\".*\" \".*\"")

        # self.stripperblocks is the list that gets returned. It will contain every block.
        self.stripperblocks = []

        mode = None
        submode = None
        in_block = False
        in_modify_block = False
        in_submode_block = False
        props = []
        list_insert = []
        list_delete = []
        list_match = []
        list_replace = []


        # Make a list called "lines".
        # Get every line in the Stripper file, strip whitespace,
        # and append them to the list.
        lines = []
        for line in self.file:
            strippedline = line.strip()
            # Ignore commented lines, newlines and empty tab lines.
            if (strippedline[:1] == ";"
               or strippedline[:1] == "#"
               or strippedline[:2] == "//"
               or strippedline == "\n"
               or strippedline == "\t"
               or strippedline == "	"):
                continue
            else:
                lines.append(strippedline)


        # Iterate through the lines list.
        for linenum, line in enumerate(lines):

            # Look for modes
            if line[:4] == "add:":
                mode = "add"
            elif line[:7] == "filter:" or line[:7] == "remove:":
                mode = "filter"
            elif line[:7] == "modify:":
                mode = "modify"
                submode = None


            # Look for sub-modes of Modify
            elif line[:6] == "match:" and mode == "modify":
                submode = "match"
            elif line[:8] == "replace:" and mode == "modify":
                submode = "replace"
            elif line[:7] == "delete:" and mode == "modify":
                submode = "delete"
            elif line[:7] == "insert:" and mode == "modify":
                submode = "insert"


            # If we find a new block and we aren't already in one,
            # set in_block to True and clear everything
            elif line == "{" and not in_block:
                in_block = True
                props = []
                list_insert = []
                list_delete = []
                list_match = []
                list_replace = []
                if mode == "modify":
                    in_modify_block = True


            # If we find the end of an Add or Filter block and we're already in one,
            # append them to the blocks list and reset in_block back to False.
            elif (line == "}" and in_block and not in_modify_block):
                in_block = False
                if mode == "filter":
                    # Don't append if empty
                    if len(props) != 0:
                        self.stripperblocks.append(["filter", props])
                elif mode == "add":
                    if len(props) != 0:
                        self.stripperblocks.append(["add", props])


            # If we find the end of a Modify block and we are already in one,
            # add its sub-blocks to the block's dictionary, then add the block to the block list.
            # Also reset in_(modify_)block back to False.
            elif (line == "}"
                  and in_block
                  and in_modify_block
                  and not in_submode_block):
                in_block = False
                in_modify_block = False

                if list_match == [] or list_match[0] == []:
                    self.consolewindow.output("> StripperBlock: ModifyBlock contains no 'match' sub-block; all entities will be matched! This may not be what you want.\n\n", "w")

                # Create a list specifically for this block
                modify_block = ["modify", {"match": [], "replace": [], "delete": [], "insert": []}]

                # Iterate through each sub-block in the same order that Stripper does
                for submode_name, submode_list in [("match", list_match),
                                                   ("replace", list_replace),
                                                   ("delete", list_delete),
                                                   ("insert", list_insert)]:
                    # Ignore non-existent and empty sub-blocks
                    if submode_list == [] or submode_list[0] == []:
                        continue
                    # Add valid sub-blocks to the dictionary
                    else:
                        modify_block[1][submode_name] = submode_list

                # Append this block to the blocks list if not empty
                if len(modify_block) < 2:
                    self.consolewindow.output("> StripperBlock: Found a Modify block with no keyvalues or sub-blocks; ignoring.\n\n", "w")
                else:
                    self.stripperblocks.append(modify_block)


            # If we find the end of a sub-block,
            # flush all submode properties to their respective lists
            elif (line == "}"
                  and in_block
                  and in_modify_block
                  and in_submode_block):
                in_submode_block = False
                if mode == "modify":
                    if submode is None:
                        props = []
                        continue
                    elif submode == "insert":
                        for prop in props:
                            list_insert.append(prop)
                        props = []
                        continue
                    elif submode == "delete":
                        for prop in props:
                            list_delete.append(prop)
                        props = []
                        continue
                    elif submode == "match":
                        for prop in props:
                            list_match.append(prop)
                        props = []
                        continue
                    elif submode == "replace":
                        for prop in props:
                            list_replace.append(prop)
                        props = []
                        continue


            # If we find the start of a sub-block, set in_submode_block to True.
            elif (line == "{"
                  and in_block
                  and in_modify_block
                  and submode is not None):
                in_submode_block = True


            # If we're in an Add or Filter block, or a sub-block,
            # and we reach a line that isn't a bracket, then it's a prop.
            # Check if it's valid with regex and add it to props list if so.
            elif ((in_block and not in_modify_block)
                  or (in_block and in_modify_block and in_submode_block)):
                if property_regex.match(line) is not None:
                    props.append(line)
                else:
                    self.consolewindow.output("> StripperBlock: Bad keyvalue format, ignoring: %s\n\n" % line, "e")
                    continue



        return self.stripperblocks
